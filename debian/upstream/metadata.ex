# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/mylibrary/issues
# Bug-Submit: https://github.com/<user>/mylibrary/issues/new
# Changelog: https://github.com/<user>/mylibrary/blob/master/CHANGES
# Documentation: https://github.com/<user>/mylibrary/wiki
# Repository-Browse: https://github.com/<user>/mylibrary
# Repository: https://github.com/<user>/mylibrary.git
