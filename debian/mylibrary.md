% mylibrary(1) | User Commands
%
% "January  8 2024"

# NAME

mylibrary - is a simple program for managing .fb2, .epub, .pdf and djvu e-book file collections. It can also works with same formats packed in zip, 7z, jar, cpio, iso, a, ar, tar, tgz, tar.gz, tar.bz2, tar.xz, rar (see notes) archives itself or packed in same types of archives with .fbd files (epub, djvu and pdf books). MyLibrary creates own database and does not change files content, names or location.

# SYNOPSIS

**mylibrary**

# DESCRIPTION

It is simple. Just create collection (see proper menu item), search book and open it (right mouse click on book). Book will be opened in default system file reader proper to each type of file. Also you can create book-marks (right mouse click on book) and read it later. Book can be removed from collection, added to collection or copied to any path you want. You can refresh collection, remove it, export or import collection database. Also you can manually edit database entries.

Notes about archives usage

1. rar archives are supported partially. This means, that you can carry out all operations, except book removing. In case of rar archive you can remove whole archive, but not separate book from it.
2. Archives, packed in another archives, are not supported. This means, that MyLibrary does not parse for example zip archive packed in another zip archive. Only exception is epub format (epub books are zip archives itself).

# AUTHOR

Partizand <partizand@gmail.com>
:   Wrote this manpage for the Debian system.

# COPYRIGHT

Copyright © 2007 Partizand

This manual page was written for the Debian system (and may be used by
others).

Permission is granted to copy, distribute and/or modify this document under
the terms of the GNU General Public License, Version 2 or (at your option)
any later version published by the Free Software Foundation.

On Debian systems, the complete text of the GNU General Public License
can be found in /usr/share/common-licenses/GPL.